(ns cljs-game-of-life.core
  (:require [reagent.core :as reagent :refer [atom]]
            [cljs.core.async :refer [<! timeout]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(enable-console-print!)

(def board-size 50)

(defn neighbours [[x y]]
  [[(+ x 1) y]
   [(- x 1) y]
   [x (+ y 1)]
   [x (- y 1)]
   [(+ x 1) (+ y 1)]
   [(+ x 1) (- y 1)]
   [(- x 1) (+ y 1)]
   [(- x 1) (- y 1)]])

(defn clear-cells [board]
  (assoc board :cells {}))

(defn randomize-board [board]
  (let [cells (loop [positions (for [x (range (:width board))
                                     y (range (:height board))]
                                 [x y])
                     cells {}]
                (if-let [position (first positions)]
                  (let [result (if (rand-nth [true false]) (assoc cells position true) cells)]
                    (recur (rest positions) result))
                  cells))]
    (assoc board :cells cells)))

(defonce app-state (atom {:board   {:height board-size :width board-size :cells {}}
                          :paused? true
                          :options {:refresh-rate 25}}))

(defn neighbours-of [cells position]
  (for [neighbour (neighbours position)]
    (get cells neighbour)))

(defn advance-position [cells position]
  (let [neighbours (neighbours-of cells position)
        neighbour-count (count (remove nil? neighbours))
        alive? (get cells position)]
    (or (= neighbour-count 3)
        (and (= neighbour-count 2) alive?))))

(defn advance-board [board]
  (let [cells
        (let [cells (get board :cells)]
          (loop [positions (for [x (range (:width board))
                                 y (range (:height board))]
                             [x y])
                 cells-result {}]
            (if-let [position (first positions)]
              (let [cells-result (if (advance-position cells position)
                                   (assoc cells-result position true)
                                   cells-result)]
                (recur (rest positions) cells-result))
              cells-result)))]
    (assoc board :cells cells)))

(defn update-board [st]
  (let [board (get st :board)
        board (if-not (get st :paused?)
                (advance-board board)
                board)]
    (assoc st :board board)))

(defn game-board [st]
  (fn []
    (let [cell-size 15]
      [:div#board
       {:style {:width (* board-size cell-size) :height (* board-size cell-size)}
        }
       (doall
         (let [board (get @st :board)
               cells (get board :cells)]
           (for [x (range (:width board))
                 y (range (:height board))
                 :let [position [x y]
                       alive? (get cells position)]]
             ^{:key (str "cell-" (+ (* x (:width board)) y))}
             [:div.cell {:style   {:height           cell-size :width cell-size
                                   :background-color (if alive? "#000" "#fff")
                                   :color            "#fff"
                                   :text-align       "center"}
                         :onClick #(swap! st update-in [:board :cells] (fn [cells] (if alive?
                                                                                     (dissoc cells position)
                                                                                     (assoc cells position true))))
                         }])))])))

(defn game [st]
  (let [board (get @st :board)
        cells (get board :cells)]
    (fn []
      [:div
       [game-board st]
       [:div#controls
        [:button
         {:type     "button"
          :on-click #(swap! st update :paused? not)}
         (if (get @st :paused?) [:i.fa.fa-play.fa-2x] [:i.fa.fa-cog.fa-2x.fa-spin.fa-fw])]
        [:button
         {:type     "button"
          :on-click #(swap! st update :board advance-board)}
         [:i.fa.fa-step-forward.fa-2x]]
        [:button
         {:type     "button"
          :on-click #(swap! st update :board randomize-board)
          }
         [:i.fa.fa-random.fa-2x]]
        [:button
         {:type     "button"
          :on-click #(do
                       ;(swap! st assoc :board (new-board board-size))
                       (swap! st update :board clear-cells))}
          [:i.fa.fa-trash.fa-2x]]]])))

(defn ^:export run []
  (reagent/render [game app-state]
                  (. js/document (getElementById "app"))))

(run)

(go
  (while true
    (swap! app-state update-board)
    (<! (timeout (get-in @app-state [:options :refresh-rate])))))